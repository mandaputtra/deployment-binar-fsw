function rootRequest(_req, res) {
  res.render('login.ejs')
}

async function login(req, res) {
  const user = req.user
  if (user.role === 'admin') {
    return res.redirect('/admin')
  } else {
    return res.redirect('/student')
  }
}

function adminPage(req, res) {
  if (req.user.role === 'admin') {
    res.render('admin.ejs')
  } else {
    res.render('limited_access.ejs')
  }
}


function studentPage(_req, res) {
  res.render('student.ejs')
}

module.exports = { rootRequest, login, adminPage, studentPage }

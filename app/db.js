const { Pool } = require('pg')

const prodDB  = {
  connectionString: process.env.DATABASE_URL,
  ssl: { rejectUnauthorized: false }
}

const localDB = {
  connectionString: process.env.DATABASE_URL,
}

const pool = new Pool(process.env.NODE_ENV === 'development' ? localDB : prodDB)

module.exports = {
  query: (text, params) => pool.query(text, params)
}
